
// opencvhw3Dlg.cpp : 實作檔
//

#include "stdafx.h"
#include "opencvhw3.h"
#include "opencvhw3Dlg.h"
#include "afxdialogex.h"
#include "opencv2\highgui\highgui.hpp"
#include "opencv\cv.h"
#include "stdio.h"
#include "string.h"


#include <vector>


using namespace cv;
using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 對 App About 使用 CAboutDlg 對話方塊

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 對話方塊資料
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支援

// 程式碼實作
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// Copencvhw3Dlg 對話方塊



Copencvhw3Dlg::Copencvhw3Dlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(Copencvhw3Dlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void Copencvhw3Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(Copencvhw3Dlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON5, &Copencvhw3Dlg::OnBnClickedButton5)
	ON_BN_CLICKED(IDC_BUTTON6, &Copencvhw3Dlg::OnBnClickedButton6)
	ON_BN_CLICKED(IDC_BUTTON7, &Copencvhw3Dlg::OnBnClickedButton7)
	ON_BN_CLICKED(IDC_BUTTON8, &Copencvhw3Dlg::OnBnClickedButton8)
	ON_BN_CLICKED(IDC_BUTTON9, &Copencvhw3Dlg::OnBnClickedButton9)
	ON_BN_CLICKED(IDC_BUTTON10, &Copencvhw3Dlg::OnBnClickedButton10)
END_MESSAGE_MAP()


// Copencvhw3Dlg 訊息處理常式

BOOL Copencvhw3Dlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 將 [關於...] 功能表加入系統功能表。

	// IDM_ABOUTBOX 必須在系統命令範圍之中。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 設定此對話方塊的圖示。當應用程式的主視窗不是對話方塊時，
	// 框架會自動從事此作業
	SetIcon(m_hIcon, TRUE);			// 設定大圖示
	SetIcon(m_hIcon, FALSE);		// 設定小圖示

	// TODO:  在此加入額外的初始設定
	AllocConsole();
	freopen("CONOUT$", "w", stdout);



	return TRUE;  // 傳回 TRUE，除非您對控制項設定焦點
}

void Copencvhw3Dlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果將最小化按鈕加入您的對話方塊，您需要下列的程式碼，
// 以便繪製圖示。對於使用文件/檢視模式的 MFC 應用程式，
// 框架會自動完成此作業。

void Copencvhw3Dlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 繪製的裝置內容

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 將圖示置中於用戶端矩形
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 描繪圖示
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// 當使用者拖曳最小化視窗時，
// 系統呼叫這個功能取得游標顯示。
HCURSOR Copencvhw3Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


//1.1
void Copencvhw3Dlg::OnBnClickedButton5()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	Mat img = imread("./1.bmp");
	Mat grayimg;
	cvtColor(img, grayimg, CV_BGR2GRAY);
	cvNamedWindow("Corner Detection", CV_WINDOW_AUTOSIZE);

	

	vector<Point2f> corners;
	bool found = findChessboardCorners(grayimg, Size(11, 8), corners);

	//cornerSubPix(grayimg, corners, Size(11, 8), Size(-1, -1), TermCriteria(CV_TERMCRIT_EPS | CV_TERMCRIT_ITER, 30, 0.1));
	drawChessboardCorners(grayimg, Size(11, 8), corners, found);


	imshow("Corner Detection", grayimg);

	cvWaitKey(0);
	cvDestroyWindow("Corner Detection");
	

}

//1.2
void Copencvhw3Dlg::OnBnClickedButton6()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	Mat img = imread("./1.bmp");
	Mat grayimg;
	cvtColor(img, grayimg, CV_BGR2GRAY);
	//cvNamedWindow("Corner Detection", CV_WINDOW_AUTOSIZE);



	vector<Point2f> corners;
	bool found = findChessboardCorners(grayimg, Size(11, 8), corners);

	cornerSubPix(grayimg, corners, Size(11, 8), Size(-1, -1), TermCriteria(CV_TERMCRIT_EPS | CV_TERMCRIT_ITER, 30, 0.1));
	//drawChessboardCorners(grayimg, Size(11, 8), corners, found);
	

	//imshow("Corner Detection", grayimg);
	printf("[%3.3f, %3.3f]\n", corners[10].x, corners[10].y);
	cvWaitKey(0);
	//cvDestroyWindow("Corner Detection");
}

//2.1
void Copencvhw3Dlg::OnBnClickedButton7()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	
	IplImage* img[21],* g_img[21];
	int i;
	img[0] = cvLoadImage("./1.bmp");
	img[1] = cvLoadImage("./2.bmp");
	img[2] = cvLoadImage("./3.bmp");
	img[3] = cvLoadImage("./4.bmp");
	img[4] = cvLoadImage("./5.bmp");
	img[5] = cvLoadImage("./6.bmp");
	img[6] = cvLoadImage("./7.bmp");
	img[7] = cvLoadImage("./8.bmp");
	img[8] = cvLoadImage("./9.bmp");
	img[9] = cvLoadImage("./10.bmp");
	img[10] = cvLoadImage("./11.bmp");
	img[11] = cvLoadImage("./12.bmp");
	img[12] = cvLoadImage("./13.bmp");
	img[13] = cvLoadImage("./14.bmp");
	img[14] = cvLoadImage("./15.bmp");
	img[15] = cvLoadImage("./16.bmp");
	img[16] = cvLoadImage("./17.bmp");
	img[17] = cvLoadImage("./18.bmp");
	img[18] = cvLoadImage("./19.bmp");
	img[19] = cvLoadImage("./20.bmp");
	img[20] = cvLoadImage("./21.bmp");

	CvMat* image_points = cvCreateMat(21 * 11 * 8, 2, CV_32FC1);
	CvMat* object_points = cvCreateMat(21 * 11 * 8, 3, CV_32FC1);
	CvMat* point_counts = cvCreateMat(21, 1, CV_32SC1);
	CvMat* intrinsic_matrix = cvCreateMat(3, 3, CV_32FC1);
	CvMat* distortion_coeffs = cvCreateMat(5, 1, CV_32FC1);

	CvPoint2D32f* corners = new CvPoint2D32f[11*8];
	int corner_count;
	int step;
	
	for (i = 0; i < 21; i++)
	{
		g_img[i] = cvCreateImage(cvGetSize(img[i]), 8, 1);
		int found = cvFindChessboardCorners(
			img[i], cvSize(11, 8) , corners, &corner_count,
			CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_FILTER_QUADS
			);
		//Get Subpixel accuracy on those corners
		cvCvtColor(img[i], g_img[i], CV_BGR2GRAY);
		cvFindCornerSubPix(g_img[i], corners, corner_count,
			cvSize(11, 8), cvSize(-1, -1), cvTermCriteria(
			CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, 30, 0.1));


		step = i*11*8;
		for (int j = step, k = 0; k<11*8; ++j, ++k) {
			CV_MAT_ELEM(*image_points, float, j, 0) = corners[k].x;
			CV_MAT_ELEM(*image_points, float, j, 1) = corners[k].y;
			CV_MAT_ELEM(*object_points, float, j, 0) = k / 11;
			CV_MAT_ELEM(*object_points, float, j, 1) = k % 11;
			CV_MAT_ELEM(*object_points, float, j, 2) = 0.0f;
		}
		CV_MAT_ELEM(*point_counts, int, i, 0) = 11*8;
	}
	CV_MAT_ELEM(*intrinsic_matrix, float, 0, 0) = 1.0f;
	CV_MAT_ELEM(*intrinsic_matrix, float, 1, 1) = 1.0f;
	
	cvCalibrateCamera2(
		object_points, image_points,
		point_counts, cvGetSize(img[0]),
		intrinsic_matrix, distortion_coeffs,
		NULL, NULL, 0 //CV_CALIB_FIX_ASPECT_RATIO
		);
	
	printf("intrinsic_matrix:\n[%f, %f, %f\n  %f, %f, %f\n  %f, %f, %f]\n"
		, CV_MAT_ELEM(*intrinsic_matrix, float, 0, 0), CV_MAT_ELEM(*intrinsic_matrix, float, 0, 1), CV_MAT_ELEM(*intrinsic_matrix, float, 0, 2)
		, CV_MAT_ELEM(*intrinsic_matrix, float, 1, 0), CV_MAT_ELEM(*intrinsic_matrix, float, 1, 1), CV_MAT_ELEM(*intrinsic_matrix, float, 1, 2)
		, CV_MAT_ELEM(*intrinsic_matrix, float, 2, 0), CV_MAT_ELEM(*intrinsic_matrix, float, 2, 1), CV_MAT_ELEM(*intrinsic_matrix, float, 2, 2));
		
}

//2.2
void Copencvhw3Dlg::OnBnClickedButton8()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	IplImage* img[21], *g_img[21];
	int i;
	img[0] = cvLoadImage("./1.bmp");
	img[1] = cvLoadImage("./2.bmp");
	img[2] = cvLoadImage("./3.bmp");
	img[3] = cvLoadImage("./4.bmp");
	img[4] = cvLoadImage("./5.bmp");
	img[5] = cvLoadImage("./6.bmp");
	img[6] = cvLoadImage("./7.bmp");
	img[7] = cvLoadImage("./8.bmp");
	img[8] = cvLoadImage("./9.bmp");
	img[9] = cvLoadImage("./10.bmp");
	img[10] = cvLoadImage("./11.bmp");
	img[11] = cvLoadImage("./12.bmp");
	img[12] = cvLoadImage("./13.bmp");
	img[13] = cvLoadImage("./14.bmp");
	img[14] = cvLoadImage("./15.bmp");
	img[15] = cvLoadImage("./16.bmp");
	img[16] = cvLoadImage("./17.bmp");
	img[17] = cvLoadImage("./18.bmp");
	img[18] = cvLoadImage("./19.bmp");
	img[19] = cvLoadImage("./20.bmp");
	img[20] = cvLoadImage("./21.bmp");

	CvMat* image_points = cvCreateMat(21 * 11 * 8, 2, CV_32FC1);
	CvMat* object_points = cvCreateMat(21 * 11 * 8, 3, CV_32FC1);
	CvMat* point_counts = cvCreateMat(21, 1, CV_32SC1);
	CvMat* intrinsic_matrix = cvCreateMat(3, 3, CV_32FC1);
	CvMat* distortion_coeffs = cvCreateMat(5, 1, CV_32FC1);

	CvPoint2D32f* corners = new CvPoint2D32f[11 * 8];
	int corner_count;
	int step;

	for (i = 0; i < 21; i++)
	{
		g_img[i] = cvCreateImage(cvGetSize(img[i]), 8, 1);
		int found = cvFindChessboardCorners(
			img[i], cvSize(11, 8), corners, &corner_count,
			CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_FILTER_QUADS
			);
		//Get Subpixel accuracy on those corners
		cvCvtColor(img[i], g_img[i], CV_BGR2GRAY);
		cvFindCornerSubPix(g_img[i], corners, corner_count,
			cvSize(11, 8), cvSize(-1, -1), cvTermCriteria(
			CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, 30, 0.1));


		step = i * 11 * 8;
		for (int j = step, k = 0; k<11 * 8; ++j, ++k) {
			CV_MAT_ELEM(*image_points, float, j, 0) = corners[k].x;
			CV_MAT_ELEM(*image_points, float, j, 1) = corners[k].y;
			CV_MAT_ELEM(*object_points, float, j, 0) = k / 11;
			CV_MAT_ELEM(*object_points, float, j, 1) = k % 11;
			CV_MAT_ELEM(*object_points, float, j, 2) = 0.0f;
		}
		CV_MAT_ELEM(*point_counts, int, i, 0) = 11 * 8;
	}
	CV_MAT_ELEM(*intrinsic_matrix, float, 0, 0) = 1.0f;
	CV_MAT_ELEM(*intrinsic_matrix, float, 1, 1) = 1.0f;


	cvCalibrateCamera2(
		object_points, image_points,
		point_counts, cvGetSize(img[0]),
		intrinsic_matrix, distortion_coeffs,
		NULL, NULL, 0 //CV_CALIB_FIX_ASPECT_RATIO
		);

	printf("dist_coeff:\n[%f, %f, %f, %f, %f]\n"
		, CV_MAT_ELEM(*distortion_coeffs, float, 0, 0), CV_MAT_ELEM(*distortion_coeffs, float, 1, 0), CV_MAT_ELEM(*distortion_coeffs, float, 2, 0)
		, CV_MAT_ELEM(*distortion_coeffs, float, 3, 0), CV_MAT_ELEM(*distortion_coeffs, float, 4, 0));

}

//2.3
void Copencvhw3Dlg::OnBnClickedButton9()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	IplImage* img[21], *g_img[21];
	int i;
	img[0] = cvLoadImage("./1.bmp");
	img[1] = cvLoadImage("./2.bmp");
	img[2] = cvLoadImage("./3.bmp");
	img[3] = cvLoadImage("./4.bmp");
	img[4] = cvLoadImage("./5.bmp");
	img[5] = cvLoadImage("./6.bmp");
	img[6] = cvLoadImage("./7.bmp");
	img[7] = cvLoadImage("./8.bmp");
	img[8] = cvLoadImage("./9.bmp");
	img[9] = cvLoadImage("./10.bmp");
	img[10] = cvLoadImage("./11.bmp");
	img[11] = cvLoadImage("./12.bmp");
	img[12] = cvLoadImage("./13.bmp");
	img[13] = cvLoadImage("./14.bmp");
	img[14] = cvLoadImage("./15.bmp");
	img[15] = cvLoadImage("./16.bmp");
	img[16] = cvLoadImage("./17.bmp");
	img[17] = cvLoadImage("./18.bmp");
	img[18] = cvLoadImage("./19.bmp");
	img[19] = cvLoadImage("./20.bmp");
	img[20] = cvLoadImage("./21.bmp");

	CvMat* image_points = cvCreateMat(21 * 11 * 8, 2, CV_32FC1);
	CvMat* object_points = cvCreateMat(21 * 11 * 8, 3, CV_32FC1);
	CvMat* point_counts = cvCreateMat(21, 1, CV_32SC1);
	CvMat* intrinsic_matrix = cvCreateMat(3, 3, CV_32FC1);
	CvMat* distortion_coeffs = cvCreateMat(5, 1, CV_32FC1);
	CvMat* rotation_vectors = cvCreateMat(21, 3, CV_32FC1);
	CvMat* translation_vectors = cvCreateMat(21, 3, CV_32FC1);

	CvPoint2D32f* corners = new CvPoint2D32f[11 * 8];
	int corner_count;
	int step;

	for (i = 0; i < 21; i++)
	{
		g_img[i] = cvCreateImage(cvGetSize(img[i]), 8, 1);
		int found = cvFindChessboardCorners(
			img[i], cvSize(11, 8), corners, &corner_count,
			CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_FILTER_QUADS
			);
		//Get Subpixel accuracy on those corners
		cvCvtColor(img[i], g_img[i], CV_BGR2GRAY);
		cvFindCornerSubPix(g_img[i], corners, corner_count,
			cvSize(11, 8), cvSize(-1, -1), cvTermCriteria(
			CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, 30, 0.1));


		step = i * 11 * 8;
		for (int j = step, k = 0; k<11 * 8; ++j, ++k) {
			CV_MAT_ELEM(*image_points, float, j, 0) = corners[k].x;
			CV_MAT_ELEM(*image_points, float, j, 1) = corners[k].y;
			CV_MAT_ELEM(*object_points, float, j, 0) = k / 11;
			CV_MAT_ELEM(*object_points, float, j, 1) = k % 11;
			CV_MAT_ELEM(*object_points, float, j, 2) = 0.0f;
		}
		CV_MAT_ELEM(*point_counts, int, i, 0) = 11 * 8;
	}
	CV_MAT_ELEM(*intrinsic_matrix, float, 0, 0) = 1.0f;
	CV_MAT_ELEM(*intrinsic_matrix, float, 1, 1) = 1.0f;


	cvCalibrateCamera2(
		object_points, image_points,
		point_counts, cvGetSize(img[0]),
		intrinsic_matrix, distortion_coeffs,
		rotation_vectors, translation_vectors, 0 //CV_CALIB_FIX_ASPECT_RATIO
		);
	
	CvMat* r1 = cvCreateMat(3, 1, CV_32FC1);
	
	CV_MAT_ELEM(*r1, float, 0, 0) = CV_MAT_ELEM(*rotation_vectors, float, 0, 0);
	CV_MAT_ELEM(*r1, float, 1, 0) = CV_MAT_ELEM(*rotation_vectors, float, 0, 1);
	CV_MAT_ELEM(*r1, float, 2, 0) = CV_MAT_ELEM(*rotation_vectors, float, 0, 2);
	
	CvMat* r3_3 = cvCreateMat(3, 3, CV_32FC1);
	cvRodrigues2(r1, r3_3, NULL);
	
	printf("extrinsic_matrix:\n[%f %f %f %f\n  %f %f %f %f\n  %f %f %f %f]\n",
		CV_MAT_ELEM(*r3_3, float, 0, 0), CV_MAT_ELEM(*r3_3, float, 0, 1), CV_MAT_ELEM(*r3_3, float, 0, 2), CV_MAT_ELEM(*translation_vectors, float, 0, 0),
		CV_MAT_ELEM(*r3_3, float, 1, 0), CV_MAT_ELEM(*r3_3, float, 1, 1), CV_MAT_ELEM(*r3_3, float, 1, 2), CV_MAT_ELEM(*translation_vectors, float, 0, 1),
		CV_MAT_ELEM(*r3_3, float, 2, 0), CV_MAT_ELEM(*r3_3, float, 2, 1), CV_MAT_ELEM(*r3_3, float, 2, 2), CV_MAT_ELEM(*translation_vectors, float, 0, 2)
		);
		
	
}

//3
void Copencvhw3Dlg::OnBnClickedButton10()
{
	// TODO:  在此加入控制項告知處理常式程式碼

	IplImage* img[21], *g_img[21];
	int i;
	img[0] = cvLoadImage("./1.bmp");
	img[1] = cvLoadImage("./2.bmp");
	img[2] = cvLoadImage("./3.bmp");
	img[3] = cvLoadImage("./4.bmp");
	img[4] = cvLoadImage("./5.bmp");
	img[5] = cvLoadImage("./6.bmp");
	img[6] = cvLoadImage("./7.bmp");
	img[7] = cvLoadImage("./8.bmp");
	img[8] = cvLoadImage("./9.bmp");
	img[9] = cvLoadImage("./10.bmp");
	img[10] = cvLoadImage("./11.bmp");
	img[11] = cvLoadImage("./12.bmp");
	img[12] = cvLoadImage("./13.bmp");
	img[13] = cvLoadImage("./14.bmp");
	img[14] = cvLoadImage("./15.bmp");
	img[15] = cvLoadImage("./16.bmp");
	img[16] = cvLoadImage("./17.bmp");
	img[17] = cvLoadImage("./18.bmp");
	img[18] = cvLoadImage("./19.bmp");
	img[19] = cvLoadImage("./20.bmp");
	img[20] = cvLoadImage("./21.bmp");

	CvMat* image_points = cvCreateMat(21 * 11 * 8, 2, CV_32FC1);
	CvMat* object_points = cvCreateMat(21 * 11 * 8, 3, CV_32FC1);
	CvMat* point_counts = cvCreateMat(21, 1, CV_32SC1);
	CvMat* intrinsic_matrix = cvCreateMat(3, 3, CV_32FC1);
	CvMat* distortion_coeffs = cvCreateMat(5, 1, CV_32FC1);
	CvMat* rotation_vectors = cvCreateMat(21, 3, CV_32FC1);
	CvMat* translation_vectors = cvCreateMat(21, 3, CV_32FC1);

	CvPoint2D32f* corners = new CvPoint2D32f[11 * 8];
	int corner_count;
	int step;

	for (i = 0; i < 21; i++)
	{
		g_img[i] = cvCreateImage(cvGetSize(img[i]), 8, 1);
		int found = cvFindChessboardCorners(
			img[i], cvSize(11, 8), corners, &corner_count,
			CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_FILTER_QUADS
			);
		//Get Subpixel accuracy on those corners
		cvCvtColor(img[i], g_img[i], CV_BGR2GRAY);
		cvFindCornerSubPix(g_img[i], corners, corner_count,
			cvSize(11, 8), cvSize(-1, -1), cvTermCriteria(
			CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, 30, 0.1));


		step = i * 11 * 8;
		for (int j = step, k = 0; k<11 * 8; ++j, ++k) {
			CV_MAT_ELEM(*image_points, float, j, 0) = corners[k].x;
			CV_MAT_ELEM(*image_points, float, j, 1) = corners[k].y;
			CV_MAT_ELEM(*object_points, float, j, 0) = k / 11;
			CV_MAT_ELEM(*object_points, float, j, 1) = k % 11;
			CV_MAT_ELEM(*object_points, float, j, 2) = 0.0f;
		}
		CV_MAT_ELEM(*point_counts, int, i, 0) = 11 * 8;
	}
	CV_MAT_ELEM(*intrinsic_matrix, float, 0, 0) = 1.0f;
	CV_MAT_ELEM(*intrinsic_matrix, float, 1, 1) = 1.0f;


	cvCalibrateCamera2(
		object_points, image_points,
		point_counts, cvGetSize(img[0]),
		intrinsic_matrix, distortion_coeffs,
		rotation_vectors, translation_vectors, 0 //CV_CALIB_FIX_ASPECT_RATIO
		);

	CvMat* r1 = cvCreateMat(3, 1, CV_32FC1);

	

	CvMat* r3_3 = cvCreateMat(3, 3, CV_32FC1);
	

	cvNamedWindow("3", CV_WINDOW_AUTOSIZE);
	i = 0;
	CvMat* p005 = cvCreateMat(3, 1, CV_32FC1);
	CvMat* p113 = cvCreateMat(3, 1, CV_32FC1);
	CvMat* pn113 = cvCreateMat(3, 1, CV_32FC1);
	CvMat* p1n13 = cvCreateMat(3, 1, CV_32FC1);
	CvMat* pn1n13 = cvCreateMat(3, 1, CV_32FC1);
	CvMat* p000 = cvCreateMat(3, 1, CV_32FC1);

	CvMat* tp005 = cvCreateMat(3, 1, CV_32FC1);
	CvMat* tp113 = cvCreateMat(3, 1, CV_32FC1);
	CvMat* tpn113 = cvCreateMat(3, 1, CV_32FC1);
	CvMat* tp1n13 = cvCreateMat(3, 1, CV_32FC1);
	CvMat* tpn1n13 = cvCreateMat(3, 1, CV_32FC1);
	CvMat* tp000 = cvCreateMat(3, 1, CV_32FC1);

	CV_MAT_ELEM(*p005, float, 0, 0) = 0.0f;
	CV_MAT_ELEM(*p005, float, 1, 0) = 0.0f;
	CV_MAT_ELEM(*p005, float, 2, 0) = 5.0f;
	
	CV_MAT_ELEM(*p113, float, 0, 0) = 1.0f;
	CV_MAT_ELEM(*p113, float, 1, 0) = 1.0f;
	CV_MAT_ELEM(*p113, float, 2, 0) = 3.0f;

	CV_MAT_ELEM(*pn113, float, 0, 0) = -1.0f;
	CV_MAT_ELEM(*pn113, float, 1, 0) = 1.0f;
	CV_MAT_ELEM(*pn113, float, 2, 0) = 3.0f;

	CV_MAT_ELEM(*p1n13, float, 0, 0) = 1.0f;
	CV_MAT_ELEM(*p1n13, float, 1, 0) = -1.0f;
	CV_MAT_ELEM(*p1n13, float, 2, 0) = 3.0f;

	CV_MAT_ELEM(*pn1n13, float, 0, 0) = -1.0f;
	CV_MAT_ELEM(*pn1n13, float, 1, 0) = -1.0f;
	CV_MAT_ELEM(*pn1n13, float, 2, 0) = 3.0f;

	CV_MAT_ELEM(*p000, float, 0, 0) = 0.0f;
	CV_MAT_ELEM(*p000, float, 1, 0) = 0.0f;
	CV_MAT_ELEM(*p000, float, 2, 0) = 0.0f;



	//while (1)
	//{
	//	CV_MAT_ELEM(*r1, float, 0, 0) = CV_MAT_ELEM(*rotation_vectors, float, i, 0);
	//	CV_MAT_ELEM(*r1, float, 1, 0) = CV_MAT_ELEM(*rotation_vectors, float, i, 1);
	//	CV_MAT_ELEM(*r1, float, 2, 0) = CV_MAT_ELEM(*rotation_vectors, float, i, 2);

	//	cvRodrigues2(r1, r3_3, NULL);

	//	cvMatMul(p005, r3_3, tp005);
	//	cvMatMul(p113, r3_3, tp113);
	//	cvMatMul(pn113, r3_3, tpn113);
	//	cvMatMul(p1n13, r3_3, tp1n13);
	//	cvMatMul(pn1n13, r3_3, tpn1n13);
	//	cvMatMul(p000, r3_3, tp000);
	//	//draw

	//	cvShowImage("3", img[0]);

	//	
	//	if (cvWaitKey(500) == 27) break;

	//}
}
